const app = require("express")();
const validate = require("./lib/validate");

app.get("/", (req, res) => {
  res.send("Hello, world");
});

app.use(validate.filterIps());

app.get("/internal", (req, res) => {
  res.sendStatus(200);
});

module.exports = app;
