const internalNetworkPattern = /172\.0\.*/;

const allowedIps = ["127.0.0.1"];

const testIP = process.env.TEST_IP;

if (testIP) {
  allowedIps.push(testIP);
}

const validate = {
  filterIps() {
    return (req, res, next) => {
      let ip;
      if (req.headers["x-forwarded-for"]) {
        ip = req.headers["x-forwarded-for"];
      } else {
        ip = req.connection.remoteAddress;
      }

      if (!this.isIpAllowed(ip)) {
        res.sendStatus(401);
        return false;
      }
      next();
    };
  },
  isIpAllowed(ip) {
    const isInternal = ip.match(internalNetworkPattern);
    return isInternal || allowedIps.includes(ip);
  },
};

module.exports = validate;
