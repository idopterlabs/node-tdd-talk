const request = require("supertest");
const app = require("./../app");
const sinon = require("sinon");

describe("GET to /", () => {
  it("returns 200", (done) => {
    request(app).get("/").expect(200).expect("Hello, world", done);
  });
});

describe("GET to /internal", () => {
  describe("not setting x-forwarded-for header", () => {
    describe("when ip is allowed", () => {
      let validate;

      beforeEach(() => {
        validate = require("./../lib/validate");
        sinon.stub(validate, "isIpAllowed").callsFake(() => true);
      });

      afterEach(() => {
        validate.isIpAllowed.restore();
      });

      it("returns 200", (done) => {
        request(app)
          .get("/internal")
          .set("x-forwarded-for", null)
          .expect(200, done);
      });
    });

    describe("when ip is NOT allowed", () => {
      let validate;

      beforeEach(() => {
        validate = require("./../lib/validate");
        sinon.stub(validate, "isIpAllowed").callsFake(() => false);
      });

      afterEach(() => {
        validate.isIpAllowed.restore();
      });

      it("returns 401", (done) => {
        request(app)
          .get("/internal")
          .set("x-forwarded-for", null)
          .expect(401, done);
      });
    });
  });

  describe("setting x-forwarded-for header", () => {
    describe("when ip is allowed", () => {
      it("returns 200", (done) => {
        request(app)
          .get("/internal")
          .set("x-forwarded-for", "127.0.0.1")
          .expect(200, done);
      });
    });

    describe("when process.env.TEST_IP is set", () => {
      it("returns 200", (done) => {
        request(app)
          .get("/internal")
          .set("x-forwarded-for", "1.2.3.4.5")
          .expect(200, done);
      });
    });

    describe("when ip is NOT allowed", () => {
      it("returns 401", (done) => {
        request(app)
          .get("/internal")
          .set("x-forwarded-for", "99.0.0.1")
          .expect(401, done);
      });
    });
  });
});
