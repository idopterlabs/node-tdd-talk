const assert = require("assert");
const validate = require("./../lib/validate");

describe("validate.isIpAllowed", () => {
  describe("127.0.0.1", () => {
    it("returns true", (done) => {
      assert.ok(validate.isIpAllowed("127.0.0.1"), "nope");
      done();
    });
  });

  describe("internal network 172.0.x.x", () => {
    it("returns true", (done) => {
      assert.ok(validate.isIpAllowed("172.0.x.x"), "nope");
      done();
    });
  });

  describe("99.0.0.1", () => {
    it("returns false", (done) => {
      assert.ok(!validate.isIpAllowed("99.0.0.1"), "nope");
      done();
    });
  });
});
